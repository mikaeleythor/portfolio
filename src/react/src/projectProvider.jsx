import React, { useState, useEffect } from "react"
import { ProjectContext } from './projectContext.jsx'

const ProjectProvider = ({ children }) => {
    //This block handles state for ProjectList API
    const projectURL = `${window.appConfig.API_URL}projects/`

    const [projects, setProjects] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [hasError, setHasError] = useState(false)

    useEffect(() => {
        const fetchProjects = async () => {
            try {
                const response = await fetch(projectURL)
                const data = await response.json()
                if (response.ok && data.length > 0) {
                    setProjects(data)
                    setIsLoading(false)
                } else { 
                    setHasError(true)
                    setIsLoading(false)
                }
            } catch(err) { 
                console.error(err)
                setHasError(true)
            }
        };
        fetchProjects();
    }, [projectURL])
    //---------------------------------------------

    const { Provider } = ProjectContext
    return (
        <Provider value={{
            projects,
            isLoading,
            hasError,
            projectURL
        }}
        >
            { children }
        </Provider>
    )
}

const withProjects = (Child) => (props) => (
    <ProjectContext.Consumer>
        {(context) => <Child {...props} {...context} />}
    </ProjectContext.Consumer>
)

export { ProjectProvider, withProjects }
