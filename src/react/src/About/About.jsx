import React, {useState, useEffect} from "react"
import DynamicDisplay from '../components/DynamicDisplay.jsx'
import './About.css'
import Error from '../components/Error.jsx'
import Loading from '../components/Loading.jsx'

export default function About (props) {

    const [about, setAbout] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [hasPhoto, setHasPhoto] = useState(false)
    const [hasError, setHasError] = useState(false)

    useEffect(() => {
        const url = `${window.appConfig.API_URL}people/`
        const fetchAbout = async () => {
            try {
                const response = await fetch(url)
                const data = await response.json()
                if (response.ok && data.length > 0) {
                    try {
                        setAbout(data[0])
                        setHasPhoto( await data[0].photos.length > 0)
                    } catch(err) {setHasError(true) }
                    setIsLoading(false)
                } else { 
                    setHasError(true)
                    setIsLoading(false)
                }
            } catch(err) { 
                console.error(err)
                setHasError(true)
            }
        };
        fetchAbout();
    }, [])

    const imageSrc = (about) => {
        const url = `${window.appConfig.API_URL}people/pic/`
        for (let i = 0; i < about.photos.length; i++){
            let object = about.photos[i]
            if (object.num === 0){
                return `${url}${object.id}/`
            }
        }
        return `${url}${about.photos[0].id}`
    }

    const Paragraphs = (props) => props.split("\n").map((paragraph, i) => (
            <p key={i}>{ paragraph }</p>
        ))

    const Lists = (props) => props.split(" - ").map((listItem, i) => (
            <li key={i}>{ listItem }</li>
        ))

    const Interval = (start, end, format) => {
        const dateMapper = (date) => {
            let dateMap = new Date(date);
            return {
                day: dateMap.getDate(),
                mon: [ 'January', 'February', 'March',
                    'April', 'May', 'June',
                    'July', 'August', 'September',
                    'October', 'November', 'December'
                ][dateMap.getMonth()],
                yr: dateMap.getFullYear()
            }
        }

        const present = (obj) => obj === 1970 ? 'Present' : obj

        let from = dateMapper(start);
        let to = dateMapper(end);

        switch (format) {
            case 'month':
                return `${from.mon} ${from.yr} - ${to.mon.isNaN() ? '' : to.mon +' '}${present(to.yr)}`
            default:
                if (from.yr === to.yr){ return from.yr } 
                else { return `${from.yr} - ${present(to.yr)}` }
        }
    }

    const Experience = (array) => array.map((item, i) => (
        <li key={i}><section>
            <h4>{ item.company }</h4>
            <h4>{ item.title }</h4>
            <p>{ Interval(item.date_start, item.date_end) }</p>
            <br/>
            { Paragraphs(item.description) }
        </section></li>
    ))

    const Education = (array) => array.map((item, i) => (
        <li key={i}><section>
            <h4>{ item.school }</h4>
            <h4>{ item.degree }</h4>
            <p>{ Interval(item.date_start, item.date_end) }</p>
            <br/>
            { Paragraphs(item.description) }
        </section></li>
    ))

    const Skills = (array) => array.map((item, i) => (
        <li key={i}><section>
            <h4>{ item.category }</h4>
            <br/>
            <ul>{ Lists( item.description ) }</ul>
        </section></li>
    ))

    if (isLoading) return ( <Loading message={'Hold on...'} /> )
    else if (hasError) return ( <Error message={"Sorry... I couldn't fetch that for you"}/> )
    else return (
        <div className='Body About'>
            <DynamicDisplay 
                topPar={about.name}
                title={'About Me'}
                src={hasPhoto && imageSrc(about)}
            />
            <article>
                <section>
                    <h4>Bio</h4>
                    { !isLoading && Paragraphs( about.description ) }                
                </section>
                <ul className='Resume'>
                    <li>
                        <h4>Experience</h4>
                        <ul>{ !isLoading && Experience( about.experience ) }</ul>
                    </li>
                    <hr/>
                    <li>
                        <h4>Skills</h4>
                        <ul>{ !isLoading && Skills( about.skills ) }</ul>
                    </li>
                    <hr/>
                    <li>
                        <h4>Education</h4>
                        <ul>{ !isLoading && Education( about.education ) }</ul>
                    </li>
                </ul>
            </article>
        </div>
    );
}
