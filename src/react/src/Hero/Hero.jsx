import React, { useContext } from "react"
import { Link } from "react-router-dom"
import './Hero.css'
import Cards from '../components/Cards.jsx'
import { ProjectContext } from '../projectContext.jsx'

export default function Hero (props) {

    const context = useContext(ProjectContext)
    const projects = context.projects
    const isLoading = context.isLoading
    const hasError = context.hasError
    const projectURL = context.projectURL

    return (
        <div className='Body Hero'>
            <div className='Display'>
                <section>
                    <p>Hey! My name is Eythor and I'm a</p>
                    <h1>Freelance Developer</h1>
                    <p><Link to='projects'>Check out my projects!</Link></p>
                </section>
            </div>
            { isLoading ? null : 
            ( 
                hasError ? null : 
                <Cards 
                    path={'projects/'} 
                    projects={projects} 
                    projectURL={projectURL} 
                />
            )}
        </div>
    );
}
