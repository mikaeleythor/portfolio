import React from "react"
import DynamicDisplay from './DynamicDisplay.jsx'

function Error({ message }){
    return(
        <div className='Body Error'>
            <DynamicDisplay 
                subPar={message}
            />
        </div>
    )
}

export default Error
