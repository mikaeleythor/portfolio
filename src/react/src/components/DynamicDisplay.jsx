import React from 'react'
import AsyncImage from './AsyncImage.jsx'

const DynamicDisplay = ({topPar, title, subPar, src}) => {

    const exists = (item) => typeof item  !== "undefined" && item !== '' && item !== null 
    return (
        <div className='Display'>
            <section>
                { exists(topPar) && 
                    <p>{topPar}</p> }
                { exists(title) && 
                    <h1>{title}</h1> }
                { exists(subPar) && 
                    <p>{subPar}</p> }
            </section>
            { 
                typeof src !== "undefined" && src !== null &&
                <div className='ImgWrapper'>
                    <AsyncImage src={ src } />
                </div>
            }
        </div>
    )
}


export default DynamicDisplay
