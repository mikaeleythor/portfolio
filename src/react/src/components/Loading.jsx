import React from "react"
import DynamicDisplay from './DynamicDisplay.jsx'

function Loading({ message }){
    return(
        <div className='Body Loading'>
            <DynamicDisplay 
                topPar={message}
            />
        </div>
    )
}

export default Loading
