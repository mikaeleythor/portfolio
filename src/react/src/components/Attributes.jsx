import Lists from './Lists.jsx'
const Attributes = (array) => {

    if (Array.isArray(array)) {
        return array.map((item, i) => {
            return (
                <li key={i}><section>
                    <h4>{ item.category }</h4>
                    <ul>{ Lists( item.description ) }</ul>
                </section></li>
            )
        })
    } else return null;
}

export default Attributes
