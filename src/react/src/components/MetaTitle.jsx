import React from "react"
import { Helmet } from "react-helmet";

function MetaTitle({title, content}){
    return(
        <Helmet>
            <title>{'Eythor Mikael - ' + title}</title>
            <meta name="description" content={content}/>
        </Helmet>
    )
}

export default MetaTitle
