import React from "react"
import { Link } from "react-router-dom"

function ButtonMailTo({ mailto, label, subject }){
    return(
        <Link
            to='#'
            onClick={(e) => { 
                window.location.href = `${mailto}?subject=${subject}`; 
                e.preventDefault(); 
            }}
        >
            {label}
        </Link>
        );
};

export default ButtonMailTo
