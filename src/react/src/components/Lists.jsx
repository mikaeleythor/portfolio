import Paragraphs from './Paragraphs.jsx'
const Lists = (props) => props.split(" - ").map((listItem, i) => {

        const isLink = item => item[0] === '[' && item[item.length-1] === ')';

        const makeLink = item => {
            let path = item.slice(item.indexOf('(')+1,item.length-1)
            let text = item.slice(1, item.indexOf(']'))
            return ( <p><a href={path}>{text}</a></p> )
        }
    return (
        <li className='AttributeItem' key={i}>
            { isLink(listItem) ? makeLink(listItem) : Paragraphs(listItem) }
        </li>
    )
})

export default Lists
