import React from "react"
import { Link } from "react-router-dom";
import '../Project/Project.css'

function Cards({projects, path, projectURL}){
    //Map an array of Project objects to an <ul> of cards
    const Items = (projects, location='') => {
        const getPicURL = (project) => {
            if (project.display === 'undefined' || project.display === null) {
                if (project.photos.length === 0) { throw Error('Project has no photos!') } 
                else { return `${projectURL}${project.photos[0].id}/photo/` }
            } else { return `${projectURL}${project.id}/display/` }
        }

        return (
            projects.map((project, i) => (
                <li className='Card' key={i}>
                    <Link to={location+project.slug} state={{project: project }}>
                        <img src={getPicURL(project)}/>
                    </Link>
                    <section>
                        <Link to={location+project.slug} state={{project: project }}>
                            <h3>{project.name}</h3>
                        </Link>
                        <p>{project.keywords}</p>
                    </section>
                </li>
            ))
        )
    }
    return( <ul className='Cards'>{Items(projects, path)}</ul> )
}

export default Cards
