const Paragraphs = (props) => props.split("\n").map((paragraph, i) => (
    <p key={i}>{ paragraph }</p>
))

export default Paragraphs

