const MonthYear = (dateStr) => {
    let date = new Date(dateStr);
    let month = [ 'January', 'February', 'March',
                'April', 'May', 'June',
                'July', 'August', 'September',
                'October', 'November', 'December'
                ][date.getMonth()];
    let year = date.getFullYear();
    return `${month} ${year}`
}

export default MonthYear
