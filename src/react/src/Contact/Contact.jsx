import React, {useState} from "react"
//import DynamicDisplay from '../components/DynamicDisplay.jsx'
import ButtonMailTo from '../components/ButtonMailTo.jsx'
import './Contact.css'

function Contact(){

    const reqURL = `${window.appConfig.API_URL}reqs/`

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [description, setDescription] = useState('');
    const [message, setMessage] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            let response = await fetch(reqURL, {
                method: "POST",
                headers: new Headers({"content-type": "application/json"}),
                body: JSON.stringify({
                    name: name,
                    email: email,
                    body: description,
                }),
            });
            if (response.status === 200) {
                setName("");
                setEmail("");
                setDescription("");
                setMessage("Your request has been received!");
            } else {
                setMessage("Some error occured");
            }
        } catch(err) {
            console.log(err)
        }
    };

    return(
        <div className='Body Contact'>
            <div className='Display'>
                <section>
                    <p>Send me an
                    <ButtonMailTo 
                        label=" email" 
                        mailto={`mailto:${window.appConfig.email}`}
                        subject="Hey! I'd like to..."/> or...</p>
                    <h1>Tell Me</h1>
                </section>
                <form onSubmit={handleSubmit}>
                    <label htmlFor='name'>your name</label>
                    <input 
                        type='text' 
                        id='name'
                        value={name} 
                        onChange={(e)=>setName(e.target.value)}
                    />
                    <label htmlFor='email'>your email</label>
                    <input 
                        type='email' 
                        id='email'
                        value={email} 
                        onChange={(e)=>setEmail(e.target.value)}
                    />
                    <label htmlFor='description'>and what I can do for you</label>
                    <textarea cols='40' rows='5'
                        id='description'
                        className='ReqDescription'
                        value={description} 
                        onChange={(e)=>setDescription(e.target.value)}
                    ></textarea>
                    <input type='submit' value='Send' />
                <div className='message'>{message && <p>{message}</p>}</div>
                </form>
            </div>
        </div>
    )
}

export default Contact
