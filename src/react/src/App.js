import React, { lazy, Suspense } from "react";
import { Routes, Route } from "react-router-dom";
import './App.css';
import Hero from './Hero/Hero.jsx';
import Header from './Header/Header.jsx';
import MetaTitle from './components/MetaTitle.jsx';
import { ProjectProvider } from './projectProvider.jsx';
const About = lazy(() => import("./About/About.jsx"));
const Contact = lazy(() => import("./Contact/Contact.jsx"));
const Project = lazy(() => import("./Project/Project.jsx"));
const ProjectList = lazy(() => import("./Project/ProjectList.jsx"));
const ProjectDetail = lazy(() => import("./Project/ProjectDetail.jsx"));

const App = () => {
    return (
        <div className="App">
            <Header />
            <Routes >
                <Route path='/' element={
                    <ProjectProvider>
                        <MetaTitle title='Portfolio' content='portfolio application'/>
                        <Hero />
                    </ProjectProvider>
                } />
                <Route path='about' element={
                    <Suspense fallback={<div>...</div>}>
                        <MetaTitle title='About me' content='nested component'/>
                        <About />
                    </Suspense>} />
                <Route path='contact' element={
                    <Suspense fallback={<div>...</div>}>
                        <MetaTitle title='Contact' content='nested component'/>
                        <Contact />
                    </Suspense>} />
                <Route path='projects/*' element={ 
                    <Suspense fallback={<div>...</div>}>
                        <Project /> 
                    </Suspense>
                    }>
                    <Route path='' element={
                        <Suspense fallback={<div>...</div>}>
                            <ProjectList />
                        </Suspense>
                    } />
                    <Route path=':projectSlug' element={
                        <Suspense fallback={<div>...</div>}>
                            <ProjectDetail />
                        </Suspense>
                    } />
                </Route>
            </Routes>
        </div>
    );
};

export default App;
