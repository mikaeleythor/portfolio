import React, {useState, useContext, useEffect} from "react"
import './Project.css'
import { ProjectContext } from '../projectContext.jsx'
import DynamicDisplay from '../components/DynamicDisplay.jsx'
import Loading from '../components/Loading.jsx'
import Error from '../components/Error.jsx'
import Cards from '../components/Cards.jsx'

function ProjectList ({ inheritedPath }) {

    const context = useContext(ProjectContext)
    const projects = context.projects
    const isLoading = context.isLoading
    const hasError = context.hasError
    const projectURL = context.projectURL

    // This block manages inherited link path
    const [path, setPath] = useState('');
    useEffect(() => {
        if (typeof inheritedPath === "undefined") setPath('');
        else setPath(inheritedPath);
    }, [inheritedPath])
    
    if (isLoading) return ( <Loading message={'Hold on...'} /> )
    else if (hasError) return ( <Error message={"Sorry... I couldn't fetch that for you"}/> )
    else return (
        <div className='Body Project'>
            <DynamicDisplay title='Projects' /> 
            <Cards path={path} projects={projects} projectURL={projectURL}/>
        </div>
    )
}

export default ProjectList
