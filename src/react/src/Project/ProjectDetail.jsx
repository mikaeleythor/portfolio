import React, {useState, useEffect} from "react"
import { useLocation } from "react-router-dom"
import ProjectDetailBlock from "./ProjectDetailBlock.jsx"
import MonthYear from "../components/MonthYear.jsx"
import DynamicDisplay from "../components/DynamicDisplay.jsx"
import Error from "../components/Error.jsx"


export default function ProjectDetail (props) {

    // This block manages data blocks inherited from location
    const {project} = useLocation().state
    const [blocks, setBlocks] = useState([])
    const [hasBlocks, setHasBlocks] = useState(false)

    useEffect(() => {

        // Label objects by type
        const addType = (array, type) => {
            return array.map((item, i) => {
                item.type = type
                return item
            })
        }

        // Helper function for sorting
        const compareNum = (a, b) => {
            if (a.num < b.num) return -1;
            else if (a.num > b.num) return 1;
            else return 0;
        }

        // Sort items into blocks
        const mapBlocks = blockItems => {
            const blocks = []
            blockItems.forEach((item, i) => {
                let num = Math.floor(item.num)
                if (num > blocks.length) blocks.push([item])
                else if (num === blocks.length) blocks[num-1].push(item)
                else throw Error('Something wrong with counting logic')
            })
            return blocks
        }

        // Label photos & chapters
        const photos = addType(project.photos, 'photo')
        const chapters = addType(project.chapters, 'chapter')
        const attributes = addType(project.attributes, 'attribute')

        // Concat and sort
        const blockItems = photos.concat(chapters).concat(attributes);
        blockItems.sort(compareNum);
        setBlocks(mapBlocks(blockItems));
        setHasBlocks(true)
    },[project])
    //---------------------------------------------
            

    if (hasBlocks) {
        return (
            <div className='Body Project'>
                <DynamicDisplay 
                    topPar={project.client}
                    title={project.name}
                    subPar={MonthYear(project.date)}
                /> 
                <article>
                    { 
                        hasBlocks && blocks.map((block, i) => (
                            <ProjectDetailBlock key={i} content={block} />
                        ))
                    }
                </article>
            </div>
        ); } else return ( <Error message={"Sorry... I couldn't fetch that for you"}/> )
}
