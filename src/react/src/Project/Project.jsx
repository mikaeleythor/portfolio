import React from "react"
import MetaTitle from '../components/MetaTitle.jsx'
import { Outlet } from "react-router-dom"
import './Project.css'
import { ProjectProvider } from '../projectProvider.jsx'

export default function Project (props) {
    return (
        <ProjectProvider>
            <MetaTitle title='Projects' content='nested component'/>
            <Outlet /> 
        </ProjectProvider>
    )
}
