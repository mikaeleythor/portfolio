import React, { useState, useEffect } from "react"
import AsyncImage from '../components/AsyncImage.jsx'
import Lists from '../components/Lists.jsx'

export default function ProjectDetailBlock (props) {

    const projectURL = `${window.appConfig.API_URL}projects/`

    const [numChapters, setNumChapters] = useState(0);
    const [numAttributes, setNumAttributes] = useState(0);
    const [numPhotos, setNumPhotos] = useState(0);

    useEffect(() => {
        const determineNums = content => content.map((item, i) => {
            switch (item.type) {
                case 'chapter':
                    setNumChapters(prevstate => prevstate + 1)
                    return null;
                case 'attribute':
                    setNumAttributes(prevstate => prevstate + 1)
                    return null;
                case 'photo':
                    setNumPhotos(prevstate => prevstate + 1)
                    return null;
                default: return null;
            }
        })
        determineNums(props.content)
    }, [props.content])

    const blockType = (numChapters, numPhotos, numAttributes) => {
        // Determine className by number of chapters and photos in block

        const noLayout = (numChapters, numPhotos, numAttributes) => {
            const pcs = `photos: ${numPhotos}`
            const chps = `chapters: ${numChapters}`
            const attrs = `attributes: ${numAttributes}`
            throw Error( `No layout for ${pcs}, ${chps}, ${attrs}`)
        }

        switch (numPhotos) {
            case 0:
                switch (numChapters) {
                    case 1: 
                        switch (numAttributes) {
                            case 0: return 'BigChapter'
                            default: return 'ChapterAndAttribute'
                        }
                    case 2: 
                        switch (numAttributes) {
                            case 0: return 'TwoChapters'
                            default: noLayout(numChapters, numPhotos, numAttributes)
                        } break;
                    default: 
                        switch (numAttributes) {
                            case 0: return 'ManyChapters'
                            default: noLayout(numChapters, numPhotos, numAttributes)
                        } break;
                }
                break;
            case 1:
                switch (numChapters) {
                    case 0: return 'BigPhoto'
                    case 1: return 'ChapterAndPhoto'
                    case 2: return 'TwoChaptersAndPhoto'
                    default: noLayout(numChapters, numPhotos, numAttributes)
                } break;
            case 2:
                switch (numChapters) {
                    case 0: return 'TwoPhotos'
                    default: noLayout(numChapters, numPhotos, numAttributes)
                } break;
            default: noLayout(numChapters, numPhotos, numAttributes)

        }
    }

    // This block is dedicated to configuring React Components
    const MapItems = (block, type) => block.map((item, i) => {
        if (item.type === type) {
            switch (type) {
                case 'chapter':
                    return (
                        <section key={i}>
                            <h4 className='ProjectTitle'>{item.title}</h4>
                            <p className='ProjectBody'>{item.body}</p>
                        </section>
                    );
                case 'attribute':
                    return (
                        <li key={i}><section>
                            <h4>{ item.category }</h4>
                            <ul>{ Lists( item.description ) }</ul>
                        </section></li>
                    );
                case 'photo':
                    return (
                        <div className='ImgWrapper' key={i}>
                            <AsyncImage src={ `${projectURL}${item.id}/photo/`} />
                        </div>
                    );
                default: return null
            }
        } else { return null }
    })
    //---------------------------------------------
    
    return (
        <div className={`Block ${blockType(numChapters, numPhotos, numAttributes)}`}>
            { numChapters > 0 && <div className={'Chapters'}>{ MapItems(props.content, 'chapter') }</div> }
            { numAttributes > 0 && <ul className={'Attributes'}>{MapItems(props.content, 'attribute') }</ul> }
            { numPhotos > 0 && <div className={'Photos'}>{ MapItems(props.content, 'photo') }</div> }
        </div>
    );
}
