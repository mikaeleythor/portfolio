import React from "react"
import { Link } from "react-router-dom"
import { NavLink } from "react-router-dom"
import './Header.css'

const activeClassName = ( isActive ) => {
    return isActive ? 'Active' : 'Inactive' 
};

const menu = [
    { link: 'home', path: '/', },
    { link: 'about', path: '/about', },
    { link: 'contact', path: '/contact', },
];

export default function Header (props) {

    const Menu = menu.map(( item, i) => (
        <li key={i}><NavLink 
            className={ ({isActive}) => activeClassName( isActive ) }
            to={item.path}>
            {item.link}
        </NavLink></li>
    ));

    return (
        <header>
            <Link to={ '/' }>{ '#!/bin/home' }</Link>
            <nav><ul>{ Menu }</ul></nav>
        </header>
    );
}
